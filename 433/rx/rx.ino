#define rfReceivePin A0 //RF Receiver pin = Analog pin 0
#define ledPin 14 //Onboard LED = digital pin 13

unsigned int data = 0; // variable used to store received data
const unsigned int upperThreshold = 1000; //upper threshold value
const unsigned int lowerThreshold = 50; //lower threshold value

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}
void loop() {
  data = analogRead(rfReceivePin); //listen for data on Analog pin 0

  if (data > upperThreshold) {
    digitalWrite(ledPin, HIGH); //If a LOW signal is received, turn LED OFF
    Serial.print("HIGH: ");
    Serial.print(data);
    Serial.println("");
  }

  if (data < lowerThreshold) {
    digitalWrite(ledPin, LOW); //If a HIGH signal is received, turn LED ON
    Serial.print("low: ");
    Serial.print(data);
    Serial.println("");
  }
}
