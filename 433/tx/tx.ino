#define TX_PIN D10
#define LED_PIN 14

void setup() {
  pinMode(TX_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  digitalWrite(TX_PIN, HIGH);
  digitalWrite(LED_PIN, HIGH);
  delay(1000);
  digitalWrite(TX_PIN, LOW);
  digitalWrite(LED_PIN, LOW);
  delay(1000);
}
