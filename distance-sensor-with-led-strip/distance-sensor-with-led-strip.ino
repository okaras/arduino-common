#include <Adafruit_NeoPixel.h>
#include <NewPing.h>
// nastavení propojovacích pinů
#define pinTrigger    D3
#define pinEcho       D4
#define maxVzdalenost 100
#define LED_PIN D7

// inicializace měřícího modulu z knihovny
NewPing sonar(pinTrigger, pinEcho, maxVzdalenost);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, LED_PIN, NEO_RGBW + NEO_KHZ800);

void setup() {
  strip.begin();
  strip.show();
  // zahájení komunikace po sériové lince
  Serial.begin(9600);
}

volatile int getDistance() {
  int distance = sonar.ping_cm();
  for (int i = 0; i < 5; i++) {
    distance += sonar.ping_cm();
    delay(10);
  }
  distance = distance / 5;

    Serial.print("Vzdalenost mezi senzorem a predmetem je ");
    Serial.print(distance);
    Serial.println(" cm.");
   return distance;
}

float getDistancePercentForString(int distance) {
  if (distance >= maxVzdalenost) {
    return 1;
  } else if (distance == 0) {
    return 0;
  } else {
    return (float) getDistance() / (float) maxVzdalenost;
  }
}

void showDistance() {
  int distance = getDistance();
  if (distance > 0) {
    float percent = getDistancePercentForString(distance);
    int leds = (int) (percent * (float) 60);
  
    for (int i = 0; i < 60; i++) {
      if (i < leds) {
        strip.setPixelColor(i, strip.Color(255, 199, 0));
      } else {
        strip.setPixelColor(i, strip.Color(0, 0, 0));
      }
    }
    strip.show();
    Serial.println(leds);
    Serial.println((int) leds);
    Serial.println("=============");
  }
}

void loop() {
  showDistance();
  delay(100);
  /*
  // načtení vzdálenosti v centimetrech do vytvořené proměnné vzdalenost
  int vzdalenost = sonar.ping_cm();
  // pauza před dalším měřením
  delay(50);
  // pokud byla detekována vzdálenost větší než 0,
  // provedeme další měření
  if (vzdalenost > 0) {
    vzdalenost = 0;
    // pro získání stabilnějších výsledků provedeme 5 měření
    // a výsledky budeme přičítat do proměnné vzdalenost
    for (int i = 0; i < 5; i++) {
      vzdalenost += sonar.ping_cm();
      delay(50);
    }
    // v proměnné vzdálenost máme součet posledních 5 měření
    // a musíme tedy provést dělení 5 pro získání průměru
    vzdalenost = vzdalenost / 5;
    // vytištění informací po sériové lince
    Serial.print("Vzdalenost mezi senzorem a predmetem je ");
    Serial.print(vzdalenost);
    Serial.println(" cm.");
  }
  // pokud byla detekována vzdálenost 0, je předmět mimo měřící rozsah,
  // tedy příliš blízko nebo naopak daleko
  else {
    Serial.println("Vzdalenost mezi senzorem a predmetem je mimo merici rozsah.");
    delay(500);
  }
  */
}
