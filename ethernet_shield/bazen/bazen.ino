#include <SPI.h>
#include <Ethernet.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "DHT.h"
#include <PubSubClient.h>
#include <ArduinoJson.h>

#define HOST_NAME "IOT: POOL"
#define TEMPERATURE_SENSOR 3
#define INTERNAL_TEMP_HUMIDITY_SENSOR 7

OneWire oneWireDS(TEMPERATURE_SENSOR);
DallasTemperature temperatureSensors(&oneWireDS);
DHT dht(INTERNAL_TEMP_HUMIDITY_SENSOR, DHT22);

const char* DEVICE_ID = "0001";
const char* DEVICE_DESCRIPTION = "Pool 1";

const char* mqttServer = "iot.kaple.cz";
const int mqttPort = 1883;
const char* mqttUser = "yourMQTTuser";
const char* mqttPassword = "yourMQTTpassword";

// nastavení MAC adresy
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 32, 70);
IPAddress gateway(192, 168, 32, 64);
EthernetServer server(80);
EthernetClient ethClient;
PubSubClient pubSubClient(ethClient);

void setup() {
  // komunikace po sériové lince rychlostí 9600 baud
  Serial.begin(9600);
  pubSubClient.setServer(mqttServer, mqttPort);

  // zapnutí komunikace s Ethernet Shieldem
  Ethernet.begin(mac, ip, gateway);
  server.begin();
  
  // výpis informace o nastavené IP adrese
  Serial.print("Server je na IP adrese: ");
  Serial.println(Ethernet.localIP());
  temperatureSensors.begin();
  dht.begin();

  // Allow the hardware to sort itself out.
  delay(1500);
}

void mqttReconnect() {
  // Loop until we're reconnected
  //while (!pubSubClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (pubSubClient.connect("arduinoClient")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      pubSubClient.publish("outTopic","hello world");
      // ... and resubscribe
      pubSubClient.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(pubSubClient.state());
    }
  //}
}

char* float2char(float input) {
  char result[8];
  dtostrf(input, 6, 2, result);
  return result;
}

void sendInternalTempHumidity() {
  float temperature = dht.readTemperature();
  float humidity = dht.readHumidity();

  if (isnan(temperature) || isnan(humidity)) {
    Serial.println("Error reading DHT");
  } else {
    Serial.print("Teplota: ");
    Serial.print(float2char(temperature));
    Serial.print(" stupnu Celsia, ");
    Serial.print("vlhkost: ");
    Serial.print(humidity);
    Serial.println(" %");

    publishData("/temperature", float2char(temperature));
    publishData("/humidity", float2char(humidity));
  }
}

void sendPoolTemperatures() {
  temperatureSensors.requestTemperatures();
  int temperature = temperatureSensors.getTempCByIndex(0);
  Serial.print("Temperature: ");
  Serial.println(temperature);  
}

void publishData(const char* topic, const char* value) {
  char buffer[512];
  DynamicJsonDocument doc(JSON_OBJECT_SIZE(3));
  doc["deviceId"] = DEVICE_ID;
  doc["deviceDescription"] = DEVICE_DESCRIPTION;
  doc["value"] = value;
  serializeJson(doc, buffer);
  pubSubClient.publish(topic, buffer);
}

void loop() {
  Serial.println(ethClient.connected());
  if (!pubSubClient.connected()) {
    mqttReconnect();
  }
  sendInternalTempHumidity();
  pubSubClient.loop();
  delay(10000); 
}
