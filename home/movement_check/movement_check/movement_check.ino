#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define LED 4
#define MOTION_SENSOR 14

const char* ssid = "loschochos";
const char* password =  "16krtecek23";
const char* mqttServer = "192.168.9.111";
const int mqttPort = 1883;

long turnOffAt = 0;
boolean notificationSent = true;

long lastMsgMotionSensor = 0;
const unsigned long publishInterval = 5000;
const unsigned long blinkPeriod = 5000;

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(MOTION_SENSOR, INPUT);
  attachInterrupt(MOTION_SENSOR, detection, RISING);
  Serial.begin(9600);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED, HIGH);
    delay(250);
    digitalWrite(LED, LOW);
    delay(250);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
  client.setServer(mqttServer, mqttPort);

  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
    if (client.connect("ESP32Client")) {
      Serial.println("connected");  
    } else {
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
    }
  }
}

void detection() {
  Serial.println("Detekce pohybu pomoci HC-SR501!"); 
  notificationSent = false;
  turnOffAt = millis() + blinkPeriod;
}

void loop() {
  client.loop();
  if (millis() >= turnOffAt) {
    digitalWrite(LED, LOW);
    
    long now = millis();
    if (now - lastMsgMotionSensor > publishInterval) {
      lastMsgMotionSensor = now;
      client.publish("home/motion/hall", "false");
    }
  } else {
    digitalWrite(LED, HIGH);
    if (notificationSent == false) {
      client.publish("home/motion/hall", "true");
      notificationSent = true;
    }
  }
}

