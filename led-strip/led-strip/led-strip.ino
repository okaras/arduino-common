#include <Adafruit_NeoPixel.h>

#define LED_PIN D9

Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, LED_PIN, NEO_RGB + NEO_KHZ800);

void setup() {
  strip.begin();
  strip.show();
}

void loop() {
  for(int i = 0; i < 20; i++) {
    strip.setPixelColor(i, strip.Color(255, 0, 0));
    strip.show();
    delay(100);
  }
}
