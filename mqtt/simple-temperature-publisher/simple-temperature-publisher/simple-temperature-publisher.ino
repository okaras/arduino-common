#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define TEMPERATURE_SENSOR_PIN D8

const char* ssid = "loschochos";
const char* password = "16krtecek23";
const char* mqtt_server = "192.168.9.111";

WiFiClient espClient;
PubSubClient client(espClient);
OneWire oneWireDS(TEMPERATURE_SENSOR_PIN);
DallasTemperature sensorDS(&oneWireDS);

long lastMsg = 0;
float temp = 0;
int interval = 5000;

void setupWifi() {
  delay(10);
  Serial.println();
  Serial.print("Pripojuji se k: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi pripojena");
  Serial.println("IP adresa: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  while(!client.connected()) {
    Serial.print("Navazuji MQTT pripojeni...");
    if(client.connect("espDHT22")) {
      Serial.print("pripojeno");
    } else {
      Serial.print("chyba, rc=");
      Serial.print(client.state());
      Serial.println(" dalsi pokus za 5 sekund...");
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(9600);
  setupWifi();
  client.setServer(mqtt_server, 1883);
  sensorDS.begin();
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  long now = millis();
  if (now - lastMsg > interval) {
    sensorDS.requestTemperatures();
    lastMsg = now;
    temp = sensorDS.getTempCByIndex(0);
    if((temp > -20) && (temp < 60)) {
      String payTemp = String(temp);
      client.publish("home/livingroom/temperature", payTemp.c_str());
    }
  }
}
