#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define SWITCH_PIN D7
#define MOTIONSENSORPIN D5
#define LIGHTSENSORPIN A0

const char* ssid = "Kaple";
const char* password =  "DoloresDolores";
const char* mqttServer = "iot.kaple.cz";
const int mqttPort = 1883;
const char* mqttUser = "yourMQTTuser";
const char* mqttPassword = "yourMQTTpassword";

long lastMsgLightSensor = 0;
long lastMsgMotionSensor = 0;
int publishInterval = 5000;
 
WiFiClient espClient;
PubSubClient client(espClient);
 
void callback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  payload[length] = '\0';
  String data = String((char*)payload);
  Serial.println(data);
  if (data == "true") {
    digitalWrite(SWITCH_PIN, LOW);
  } else {
    digitalWrite(SWITCH_PIN, HIGH);
  }
 
  Serial.println();
  Serial.println("-----------------------");
 
}
 
void setup() {
  Serial.begin(9600);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
    if (client.connect("ESP32Client")) {
      Serial.println("connected");  
    } else {
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
    }
  }
 
  client.subscribe("/home/switch/1");
  pinMode(SWITCH_PIN, OUTPUT);
  digitalWrite(SWITCH_PIN, HIGH);
  pinMode(LIGHTSENSORPIN, INPUT);
  pinMode(MOTIONSENSORPIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(MOTIONSENSORPIN), motionSensorCallback, RISING);
}

void motionSensorCallback() {
  Serial.println("Detekce pohybu pomoci HC-SR501!");
  client.publish("home/motion/livingroom", "true");
  yield();
}

void publishLightSensorData() {
  long now = millis();
  if (now - lastMsgLightSensor > publishInterval) {
    lastMsgLightSensor = now;
    String data = (String) analogRead(LIGHTSENSORPIN);
    client.publish("home/light/livingroom", data.c_str());
    Serial.print("Sent: ");
    Serial.println(data);
  }
}
 
void loop() {
  client.loop();
  publishLightSensorData();

  long now = millis();
  if (now - lastMsgMotionSensor > publishInterval) {
    lastMsgMotionSensor = now;
    client.publish("home/motion/livingroom", "false");
  }
}
